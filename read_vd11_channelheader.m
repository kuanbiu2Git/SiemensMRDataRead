% READ_VB13_MDH - Read a meas.asc measurement data header from an open file
%                 Works for VB13 software version
%
% [header, ok, nBytesRead] = read_vb13_mdh(fid)
%
% header: struct with entries
% - ulTypeAndChannelLength            // 0: (1)  8bit type (0x02 => ChannelHeader)                
% - lMeasUID                          // 4: (4) measurement user ID                               
% - ulScanCounter                     // 8: (4) scan counter [1...]                               
% - ulReserved1                       // 12:(4) reserved                                          
% - ulUnused1                         // 16:(4) unused                                            
% - ulUnused2                         // 20:(4) unused                                            
% - ulChannelId                       // 24:(4) unused                                            
% - ulUnused3                         // 26:(2) unused                                            
% - ulCRC                             // 28:(4) CRC32 checksum of channel header                  
% total length:  32 byte
%
% ok: true if header read was successful, false if not
%
% nBytesRead: number of bytes actually read; full mdh length if successful, potentially less if not
%
% fid: file ID for reading
%
% Stephan.Kannengiesser@siemens.com
%
% -----------------------------------------------------------------------------
%   Copyright (C) Siemens AG 2003  All Rights Reserved. Confidential.
% -----------------------------------------------------------------------------

function [header, ok, nBytesRead] = read_vd11_channelheader(fid)

nBytesRead           = 0;

ok                   = true;

%typedef struct sChannelHeader
% {
%  PACKED_MEMBER( uint32_t,     ulTypeAndChannelLength        );    // 0: (1)  8bit type (0x02 => ChannelHeader)                
%                                                                   //    (3) 24bit channel length (header+data) in byte        
[header.ulTypeAndChannelLength, count] = fread(fid, 1,      'uint32');
nBytesRead = nBytesRead + count * 4;
if count ~= 1
    ok = false;
    return
end

%  PACKED_MEMBER( int32_t,      lMeasUID                      );    // 4: (4) measurement user ID
[header.lMeasUID, count]               = fread(fid, 1,      'int32');
nBytesRead = nBytesRead + count * 4;
if count ~= 1
    ok = false;
    return
end

%  PACKED_MEMBER( uint32_t,     ulScanCounter                 );    // 8: (4) scan counter [1...]                               
[header.ulScanCounter, count]          = fread(fid, 1,      'uint32');
nBytesRead = nBytesRead + count * 4;
if count ~= 1
    ok = false;
    return
end

%  PACKED_MEMBER( uint32_t,     ulReserved1                   );    // 12:(4) reserved                                          
[header.ulReserved1, count]            = fread(fid, 1,      'uint32');
nBytesRead = nBytesRead + count * 4;
if count ~= 1
    ok = false;
    return
end

%  PACKED_MEMBER( uint32_t,     ulUnused1                     );    // 16:(4) unused                                            
[header.ulUnused1, count]              = fread(fid, 1,      'uint32');
nBytesRead = nBytesRead + count * 4;
if count ~= 1
    ok = false;
    return
end

%  PACKED_MEMBER( uint32_t,     ulUnused2                     );    // 20:(4) unused                                            
[header.ulUnused2, count]              = fread(fid, 1,      'uint32');
nBytesRead = nBytesRead + count * 4;
if count ~= 1
    ok = false;
    return
end

%  PACKED_MEMBER( uint16_t,     ulChannelId                   );    // 24:(4) unused                                            
[header.ulChannelId, count]            = fread(fid, 1,      'uint16');
nBytesRead = nBytesRead + count * 2;
if count ~= 1
    ok = false;
    return
end

%  PACKED_MEMBER( uint16_t,     ulUnused3                     );    // 26:(2) unused                                            
[header.ulUnused3, count]              = fread(fid, 1,      'uint16');
nBytesRead = nBytesRead + count * 2;
if count ~= 1
    ok = false;
    return
end

%  PACKED_MEMBER( uint32_t,     ulCRC                         );    // 28:(4) CRC32 checksum of channel header                  
[header.ulCRC, count]                  = fread(fid, 1,      'uint32');
nBytesRead = nBytesRead + count * 4;
if count ~= 1
    ok = false;
    return
end

%} sChannelHeader;                                                  // total length:  32 byte

