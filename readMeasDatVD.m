% [input parameters]
% cFile           : index of file in super-raid [1...nFiles], default: 1
%
%   modes
%   - 'STD'         : all loop counters are used
%   - 'TSE'         : segments ignored since redundant with lines (default)
%   - 'DRY'         : only print max loop counters
%   - 'ADJ_FRE'     : read frequency adjustment scan(s) from the AdjFre measurement
%   - 'ADJ_MDS_FRE' : read frequency adjustment scans from the AdjMds measurement
%   - 'UTE'         : special mode reading selected echo and channel indices (may also be useful for 128channel data sets)
%
%   undoOS          : 0 = preserve ADC oversampling (2x); 1 = remove
%   (default)
%
% [output parameters]
% % readMeasDatVD
%     headerLength: the total Length of the header should be 
%       DataLength: the total Length of the Data part should be 8*headerRHPRaw.rows*headerRHPRaw.cols
%             rows: the Rows of the Data
%             cols: the Columns of the Data
%        dwellTime: the dwellTime of the Data (Unit: us)
%               tr: the time of repeat of the Data Unit: us)

% % dataMatrix    :  Data Matrix with complex element
%
% Author: bin.kuang@siemens.com
% -----------------------------------------------------------------------------
%   Copyright (C) Siemens SSMR 2015  All Rights Reserved. Confidential.
% -------------------------------------------------------------------------
% ----
function [dataMatrix ,scanheaderInfo ] = readMeasDatVD(nfile, cFile, mode, undoOS, iSpeCH)
%tyPixDate=0;% 1:unit8 2: unit16
%tyShadow =0;% 1:dcm from patient browser 2: dcm from RHP export.
%KnownRow=1;
%KnownCol=256;
%test readRHPRaw  function
 disp(['Number of function input arguments is ' num2str(nargin)]); 
 scanheaderInfo=read_meas_vd11scanheader(nfile,cFile, mode);
 if (nargin<5)
     iSpeCH = 1;
     dataMatrix=read_meas_vd11(nfile,cFile, mode, undoOS);
 else
     dataMatrix=read_meas_vd11_CH(nfile,cFile, mode, undoOS,iSpeCH);
 end
