% READ_MEAS_VD11 - read meas.dat file along all dimensions for software version VD11
%
% function [raw noise ref phasecor pc_order centerlines lastheader rest] = ...
%           read_meas_vb13(filename [,cFile] [,mode] [,undoOS] [,repWise] [,average] [, extRefOnly])
%
%   raw             : rawdata
%
%   noise           : noise adjust scan (for iPAT measurements)
%
%   ref             : iPAT reference lines (*currently not filled*)
%
%   phasecor        : TSE / EPI phase correction scans (*not thoroughly
%                       tested, use with care!*)
%
%   pc_order        : experimental output
%
%   centerlines     : line index / indices of k-space center lines
%                       (may contain an additional 1 for TSE due to refScan)
%
%   lastheader      : last measurement data header as read by this function
%                       (for writing / duplicating meas.out)
%
%   rest            : binary data for the rest of the meas.out file
%                       (for writing / duplicating meas.out)
%
%   cFile           : index of file in super-raid [1...nFiles], default: 1
%
%   modes
%   - 'STD'         : all loop counters are used
%   - 'TSE'         : segments ignored since redundant with lines (default)
%   - 'DRY'         : only print max loop counters
%   - 'ADJ_FRE'     : read frequency adjustment scan(s) from the AdjFre measurement
%   - 'ADJ_MDS_FRE' : read frequency adjustment scans from the AdjMds measurement
%   - 'UTE'         : special mode reading selected echo and channel indices (may also be useful for 128channel data sets)
%
%   undoOS          : 0 = preserve ADC oversampling (2x); 1 = remove (default)
%
%   repWise         : 1 = put multiple measurements (repetitions) into
%                       separate files (for large data sets), default = 0
%                     2 = put multiple phases into separate files
%
%   average         : 0 = preserve averages in their own matrix dimension
%                     1 = accumulate raw data across AVE dimension (default)
%
%   extRefOnly      : 1 = extract only "EXTRA" reference lines for iPAT
%
%
% Stephan.Kannengiesser@siemens.com
%
% -----------------------------------------------------------------------------
%   Copyright (C) Siemens AG 2009  All Rights Reserved. Confidential.
% -----------------------------------------------------------------------------
%
function [scanheaderInfo] = ...
    read_meas_vd11scanheader(filename, cFile, mode)

disp(['* This is read_meas_vd11, version 1.1.2'])

% Defines
PATNAMLEN                = 64; % Max length of patient name
PROTNAMLEN               = 64; % Max length of protocol name
MR_PARC_RAID_ALLDATA     = 0;  % normal daVinci MDH file (*.dat)
MR_PARC_RAID_MDHONLY     = 1;  % �compact?MDH file (Messdaten removed) (*.mdh)
MR_PARC_RAID_HDONLY      = 2;  % MDH file only with the header (meas.evp, phoenix.evp)
MR_PARC_RAID_LEGACY_THR  = 32; % pre VD11A MDH with empty measurement header
% > MR_PARC_RAID_LEGACY_THR    - pre VD11A MDH with (meas.evp, phoenix.evp,?

MRPARCRAID_SECT_ALIGN    = 512; % data size alignment

% find default parameters
% - filename is first
% - to facilitate the adding of later parameters, work in order only
currArg = 2;
if nargin < currArg
  cFile = 1;
end
currArg = currArg + 1;
if nargin < currArg
  mode = 'TSE';
end


% Shortcuts
is_adj_mds_fre = strcmpi(mode, 'ADJ_MDS_FRE') | strcmpi(mode, 'ADJ_FRE_MDS');

% first pass: open file and find out about dimensions as well as headers
maxLC  = zeros([1 14]);
maxCOL = 0;
maxCHA = 0;

centerlines = zeros([1 0]);
centerpars  = zeros([1 0]);

% Open the file
fid = fopen(filename,'rb');

% byte count - for alignment
bc = 0;

% First four bytes contain the version
superId = fread(fid, 1, 'uint32'); bc = bc + 4;

if superId < 32
   disp(['* first 4 bytes: ' num2str(superId) ' -> this is a VD11 data file']); 
else
   disp(['* first 4 bytes: ' num2str(superId) ' -> this is NOT a VD11 data file, aborting...']); 
   raw = [];
   return
end

% Second four bytes contain number of files in "super-RAID"
nFiles = fread(fid, 1, 'uint32'); bc = bc + 4;

disp(['* super-RAID file contains ' num2str(nFiles) ' data file(s)']);

if cFile > nFiles
    disp(['*** cFile (' num2str(cFile) ') > nFiles (' num2str(nFiles) ...
        '), aborting']);
    raw = [];
    return
end

% Loop over measurement data file headers
measId  = zeros([nFiles 1]);
fileId  = zeros([nFiles 1]);
fileOff = zeros([nFiles 1]);
fileLen = zeros([nFiles 1]);
for cF = 1:nFiles

    % Read initial information from this measurement file
    measId(cF)  = fread(fid, 1, 'uint32'); bc = bc + 4; % Used for MDH file identification
    fileId(cF)  = fread(fid, 1, 'uint32'); bc = bc + 4; % ? Number of measurements in file
    fileOff(cF) = fread(fid, 1, 'uint32'); bc = bc + 4; % offset of measurement from start
    fileLen(cF) = fread(fid, 1, 'uint32'); bc = bc + 4; % length of measurement
    dummy1         = fread(fid, 1, 'uint32'); bc = bc + 4; % experimental: 
    dummy2         = fread(fid, 1, 'uint32'); bc = bc + 4; % experimental: 
    
    disp(['* File # ' num2str(cF)]);
    disp(['- measId  : ' num2str(measId(cF))]);
    disp(['- fileId  : ' num2str(fileId(cF))]);
    disp(['- fileOff : ' num2str(fileOff(cF))]);
    disp(['- fileLen : ' num2str(fileLen(cF))]);
    disp(['- dummy1  : ' num2str(dummy1)]);
    disp(['- dummy2  : ' num2str(dummy2)]);
    
    patName    = fread(fid, [1 PATNAMLEN],  'char'); bc = bc + PATNAMLEN;  % Patient name
    protName   = fread(fid, [1 PROTNAMLEN], 'char'); bc = bc + PROTNAMLEN; % Protocol name
    
    disp(['- patName : ' patName]);
    disp(['- protName: ' protName]);

end % for cF

% Align superHeader to MRPARCRAID_SECT_ALIGN
% - this is probably unnecessary, we have the offset value
nBytesNeeded = MRPARCRAID_SECT_ALIGN * ceil(bc/MRPARCRAID_SECT_ALIGN);

disp(['* byte count so far is ' num2str(bc) ' bytes -> patch to multiple of ' ...
    num2str(MRPARCRAID_SECT_ALIGN) ' -> ' num2str(nBytesNeeded)]);

fread(fid, nBytesNeeded-bc, 'uint8');
bc = nBytesNeeded;

disp(['- file is positioned at ' num2str(ftell(fid))]);

% Don't continue for more than one file (at the moment)
%if ( nFiles > 1 )
%    disp('* currently only 1 file supported, aborting...');
%    raw = [];
%    return
%end

% Do we count the super header?
nBytesToRead = fileOff(cFile) - bc;

disp(['* continue to read until offset of measurement sub-file ' num2str(cFile) ...
    ' = ' num2str(fileOff(cFile)) ' -> ' num2str(nBytesToRead) ' bytes']);

fread(fid, nBytesToRead, 'char');

% Now there should be an ordinary measurement, including its own header
%  (evp etc.)
% -> skip that similar to a VB11 type measurement
nBytesFileHeader = fread(fid, 1, 'uint32');

disp(['* Measurement ' num2str(cFile) ' has a header of ' num2str(nBytesFileHeader) ' bytes length']);

fread(fid, nBytesFileHeader-4, 'uint8');

% Finally, we should have reached the first data scan!

% Read one scan header
%[scanheader, ok, nBytesRead] = read_vd11_scanheader(fid);

%disp(['* Reading scan header, ok = ' num2str(ok) ', nBytesRead = ' num2str(nBytesRead)]);
%disp(['- scanheader.ushSamplesInScan: ' num2str(scanheader.ushSamplesInScan)]);
%disp('- Scan header:'); scanheader


  [scanheaderInfo ok] = read_vd11_scanheader(fid);
  if ok
    mask        = evalInfoMask(scanheaderInfo.aulEvalInfoMask(1));
  else 
    disp(['*** header read not ok after ' num2str(nScansRead) ' scans - aborting read'])
  end
  

fclose(fid);
