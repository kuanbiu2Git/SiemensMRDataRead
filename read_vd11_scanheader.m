% READ_VD11_SCANHEADER - Read a scan header from an open file
%                        Works for VD11 software version
%
% [header, ok, nBytesRead] = read_vd11_scanheader(fid)
%
% header: struct with entries
% - ulFlagsAndDMALength                               //  0: ( 4) bit  0..24: DMA length [bytes]
% - lMeasUID                                          //  4: ( 4) measurement user ID                              
% - ulScanCounter                                     //  8: ( 4) scan counter [1...]                              
% - ulTimeStamp                                       // 12: ( 4) time stamp [2.5 ms ticks since 00:00]           
% - ulPMUTimeStamp                                    // 16: ( 4) PMU time stamp [2.5 ms ticks since last trigger]
% - ushSystemType                                     // 20: ( 2) System type (todo: values?? ####)               
% - ulPTABPosDelay                                    // 22: ( 2) PTAb delay ??? TODO: How do we handle this ####
% - lPTABPosX                                         // 24: ( 4) absolute PTAB position in [0.1 mm]              
% - lPTABPosY                                         // 28: ( 4) absolute PTAB position in [0.1 mm]              
% - lPTABPosZ                                         // 32: ( 4) absolute PTAB position in [0.1 mm]              
% - ulReserved1                                       // 36: ( 4) reserved for future hardware signals             
% - aulEvalInfoMask[MDH_NUMBEROFEVALINFOMASK]);       // 40: ( 8) evaluation info mask field                      
% - ushSamplesInScan                                  // 48: ( 2) # of samples acquired in scan                   
% - ushUsedChannels                                   // 50: ( 2) # of channels used in scan                         
% - sLC                                               // 52: (28) loop counters                                   
% - sCutOff                                           // 80: ( 4) cut-off values                                  
% - ushKSpaceCentreColumn                             // 84: ( 4) centre of echo                                  
% - ushCoilSelect                                     // 86: ( 2) Bit 0..3: CoilSelect                            
% - fReadOutOffcentre                                 // 88: ( 4) ReadOut offcenter value                         
% - ulTimeSinceLastRF                                 // 92: ( 4) Sequence time stamp since last RF pulse         
% - ushKSpaceCentreLineNo                             // 96: ( 2) number of K-space centre line                   
% - ushKSpaceCentrePartitionNo                        // 98: ( 2) number of K-space centre partition              
% - sSD                                               // 100:(28) Slice Data                                         
% - aushIceProgramPara[MDH_NUMBEROFICEPROGRAMPARA] ); // 128:( 8) free parameter for IceProgram       
% - aushFreePara[MDH_FREEHDRPARA] );                  // 136:( 8) free parameter                                 
% - cApplicationParameter[44]     );                  // 144:(44) unused (padding to next 192byte alignment )    
% - ulCRC                         );                  // 188:( 4) CRC 32 checksum
% total length: 6 x 32 Byte (192 Byte)                
%
% ok: true if header read was successful, false if not
%
% nBytesRead: number of bytes actually read; full mdh length if successful, potentially less if not
%
% fid: file ID for reading
%
% Stephan.Kannengiesser@siemens.com
%
% -----------------------------------------------------------------------------
%   Copyright (C) Siemens AG 2009  All Rights Reserved. Confidential.
% -----------------------------------------------------------------------------

function [header, ok, nBytesRead] = read_vd11_scanheader(fid)

MDH_NUMBEROFEVALINFOMASK   = 2;
MDH_NUMBEROFICEPROGRAMPARA = 4;
MDH_FREEHDRPARA            = 4;

% Save in header struct
header.MDH_NUMBEROFEVALINFOMASK   = MDH_NUMBEROFEVALINFOMASK;
header.MDH_NUMBEROFICEPROGRAMPARA = MDH_NUMBEROFICEPROGRAMPARA;
header.MDH_FREEHDRPARA            = MDH_FREEHDRPARA;

nBytesRead                    = 0;  % byte count

ok                   = true;

%typedef struct sScanHeader
% {                                                                                     
%  PACKED_MEMBER( uint32_t,     ulFlagsAndDMALength           );                 //  0: ( 4) bit  0..24: DMA length [bytes]
%                                                                                //          bit     25: pack bit
%                                                                                //          bit 26..31: pci_rx enable flags                      
[header.ulFlagsAndDMALength, count] = fread(fid, 1,      'uint32');  
if count == 1
    %disp('* read ok')
    nBytesRead = nBytesRead + 4;
else
    %disp('* read NOT ok')
    ok = false;
    return
end

%  PACKED_MEMBER( int32_t,      lMeasUID                      );                 //  4: ( 4) measurement user ID                              
[header.lMeasUID, count]            = fread(fid, 1,      'int32');
if count == 1
    nBytesRead = nBytesRead + 4;
else
    ok = false;
    return
end

%  PACKED_MEMBER( uint32_t,     ulScanCounter                 );                 //  8: ( 4) scan counter [1...]                              
[header.ulScanCounter, count]       = fread(fid, 1,      'uint32');
if count == 1
    nBytesRead = nBytesRead + 4;
else
    ok = false;
    return
end

%  PACKED_MEMBER( uint32_t,     ulTimeStamp                   );                 // 12: ( 4) time stamp [2.5 ms ticks since 00:00]
[header.ulTimeStamp, count]         = fread(fid, 1,      'uint32');
if count == 1
    nBytesRead = nBytesRead + 4;
else
    ok = false;
    return
end

%  PACKED_MEMBER( uint32_t,     ulPMUTimeStamp                );                 // 16: ( 4) PMU time stamp [2.5 ms ticks since last trigger]
[header.ulPMUTimeStamp, count]      = fread(fid, 1,      'uint32');
if count == 1
    nBytesRead = nBytesRead + 4;
else
    ok = false;
    return
end

%  PACKED_MEMBER( uint16_t,     ushSystemType                 );                 // 20: ( 2) System type (todo: values?? ####)               
[header.ushSystemType, count]      = fread(fid, 1,      'uint16');
if count == 1
    nBytesRead = nBytesRead + 2;
else
    ok = false;
    return
end

%  PACKED_MEMBER( uint16_t,     ulPTABPosDelay                );                 // 22: ( 2) PTAb delay ??? TODO: How do we handle this ####
[header.ushPTABPosDelay, count]    = fread(fid, 1,      'uint16');
if count == 1
    nBytesRead = nBytesRead + 2;
else
    ok = false;
    return
end

%  PACKED_MEMBER( int32_t,	   lPTABPosX                     );                 // 24: ( 4) absolute PTAB position in [0.1 mm]              
[header.lPTABPosX, count]          = fread(fid, 1,      'int32');
if count == 1
    nBytesRead = nBytesRead + 4;
else
    ok = false;
    return
end

%  PACKED_MEMBER( int32_t,	   lPTABPosY                     );                 // 28: ( 4) absolute PTAB position in [0.1 mm]              
[header.lPTABPosY, count]          = fread(fid, 1,      'int32');
if count == 1
    nBytesRead = nBytesRead + 4;
else
    ok = false;
    return
end

%  PACKED_MEMBER( int32_t,	   lPTABPosZ                     );                 // 32: ( 4) absolute PTAB position in [0.1 mm]              
[header.lPTABPosZ, count]          = fread(fid, 1,      'int32');
if count == 1
    nBytesRead = nBytesRead + 4;
else
    ok = false;
    return
end

%  PACKED_MEMBER( uint32_t,	   ulReserved1                   );                 // 36: ( 4) reserved for future hardware signals             
[header.ulReserved1, count]        = fread(fid, 1,      'uint32');
if count == 1
    nBytesRead = nBytesRead + 4;
else
    ok = false;
    return
end

%  PACKED_MEMBER( uint32_t,     aulEvalInfoMask[MDH_NUMBEROFEVALINFOMASK]);      // 40: ( 8) evaluation info mask field                      
[header.aulEvalInfoMask, count]     = fread(fid, [1 MDH_NUMBEROFEVALINFOMASK],  'uint32');
nBytesRead = nBytesRead + 4 * count;
if count ~= MDH_NUMBEROFEVALINFOMASK
    ok = false;
    return
end

%  PACKED_MEMBER( uint16_t,     ushSamplesInScan              );                 // 48: ( 2) # of samples acquired in scan                   
[header.ushSamplesInScan, count]    = fread(fid, 1,      'uint16');
if count == 1
    nBytesRead = nBytesRead + 2;
else
    ok = false;
    return
end

%  PACKED_MEMBER( uint16_t,     ushUsedChannels               );                 // 50: ( 2) # of channels used in scan                         
[header.ushUsedChannels, count]     = fread(fid, 1,      'uint16');
if count == 1
    nBytesRead = nBytesRead + 2;
else
    ok = false;
    return
end

%  PACKED_MEMBER( sLoopCounter, sLC                           );                 // 52: (28) loop counters                                   
%typedef struct {
%  PACKED_MEMBER( uint16_t,  ushLine         ); /* line index                   */
%  PACKED_MEMBER( uint16_t,  ushAcquisition  ); /* acquisition index            */
%  PACKED_MEMBER( uint16_t,  ushSlice        ); /* slice index                  */
%  PACKED_MEMBER( uint16_t,  ushPartition    ); /* partition index              */
%  PACKED_MEMBER( uint16_t,  ushEcho         ); /* echo index                   */
%  PACKED_MEMBER( uint16_t,  ushPhase        ); /* phase index                  */
%  PACKED_MEMBER( uint16_t,  ushRepetition   ); /* measurement repeat index     */
%  PACKED_MEMBER( uint16_t,  ushSet          ); /* set index                    */
%  PACKED_MEMBER( uint16_t,  ushSeg          ); /* segment index  (for TSE)     */
%  PACKED_MEMBER( uint16_t,  ushIda          ); /* IceDimension a index         */
%  PACKED_MEMBER( uint16_t,  ushIdb          ); /* IceDimension b index         */
%  PACKED_MEMBER( uint16_t,  ushIdc          ); /* IceDimension c index         */
%  PACKED_MEMBER( uint16_t,  ushIdd          ); /* IceDimension d index         */
%  PACKED_MEMBER( uint16_t,  ushIde          ); /* IceDimension e index         */
%} sLoopCounter;                                /* sizeof : 28 byte             */
[header.sLC, count]                        = fread(fid, [1 14], 'uint16');
nBytesRead = nBytesRead + 2 * count;
if count ~= 14
    ok = false;
    return
end

% make shortcuts (can we do C++ style references for header patching?)
header.cLin = header.sLC(1);
header.cAcq = header.sLC(2);
header.cSlc = header.sLC(3);
header.cPar = header.sLC(4);
header.cEco = header.sLC(5);
header.cPhs = header.sLC(6);
header.cRep = header.sLC(7);
header.cSet = header.sLC(8);
header.cSeg = header.sLC(9);
header.cIda = header.sLC(10);
header.cIdb = header.sLC(11);
header.cIdc = header.sLC(12);
header.cIdd = header.sLC(13);
header.cIde = header.sLC(14);

%  PACKED_MEMBER( sCutOffData,  sCutOff                       );                 // 80: ( 4) cut-off values                                  
%typedef struct{
%  PACKED_MEMBER( uint16_t,  ushPre          );    /* write ushPre zeros at line start */
%  PACKED_MEMBER( uint16_t,  ushPost         );    /* write ushPost zeros at line end  */
% } sCutOffData;
[header.sCutOff, count]                    = fread(fid, [1 2],  'uint16');
nBytesRead = nBytesRead + 2 * count;
if count ~= 2
    ok = false;
    return
end

%  PACKED_MEMBER( uint16_t,     ushKSpaceCentreColumn         );                 // 84: ( 4) centre of echo                                  
[header.ushKSpaceCentreColumn, count]      = fread(fid, 1,      'uint16');
if count == 1
    nBytesRead = nBytesRead + 2;
else
    ok = false;
    return
end

%  PACKED_MEMBER( uint16_t,     ushCoilSelect                 );                 // 86: ( 2) Bit 0..3: CoilSelect                            
[header.ushCoilSelect, count]              = fread(fid, 1,      'uint16');
if count == 1
    nBytesRead = nBytesRead + 2;
else
    ok = false;
    return
end

%  PACKED_MEMBER( float,        fReadOutOffcentre             );                 // 88: ( 4) ReadOut offcenter value                         
[header.fReadOutOffcentre, count]          = fread(fid, 1,      'float');
if count == 1
    nBytesRead = nBytesRead + 4;
else
    ok = false;
    return
end

%  PACKED_MEMBER( uint32_t,     ulTimeSinceLastRF             );                 // 92: ( 4) Sequence time stamp since last RF pulse         
[header.ulTimeSinceLastRF, count]          = fread(fid, 1,      'uint32');
if count == 1
    nBytesRead = nBytesRead + 4;
else
    ok = false;
    return
end

%  PACKED_MEMBER( uint16_t,     ushKSpaceCentreLineNo         );                 // 96: ( 2) number of K-space centre line                   
[header.ushKSpaceCentreLineNo, count]      = fread(fid, 1,      'uint16');
if count == 1
    nBytesRead = nBytesRead + 2;
else
    ok = false;
    return
end

%  PACKED_MEMBER( uint16_t,     ushKSpaceCentrePartitionNo    );                 // 98: ( 2) number of K-space centre partition              
[header.ushKSpaceCentrePartitionNo, count] = fread(fid, 1,      'uint16');
if count == 1
    nBytesRead = nBytesRead + 2;
else
    ok = false;
    return
end

%  PACKED_MEMBER( sSliceData,   sSD                           );                 // 100:(28) Slice Data                                         
%typedef struct {
%  PACKED_MEMBER( float,  flSag          );
%  PACKED_MEMBER( float,  flCor          );
%  PACKED_MEMBER( float,  flTra          );
%} sVector;
%typedef struct{
%  PACKED_MEMBER( sVector,         sSlicePosVec     ); /* slice position vector        */
%  PACKED_MEMBER( float,           aflQuaternion[4] ); /* rotation matrix as quaternion*/
%} sSliceData;                                         /* sizeof : 28 byte             */
[header.sSD, count]                        = fread(fid, [1 7],  'float');
nBytesRead = nBytesRead + 4 * count;
if count ~= 7
    ok = false;
    return
end

%  PACKED_MEMBER( uint16_t,     aushIceProgramPara[MDH_NUMBEROFICEPROGRAMPARA] );// 128:( 8) free parameter for IceProgram       
[header.aushIceProgramPara, count]         = fread(fid, [1 MDH_NUMBEROFICEPROGRAMPARA],  'uint16');
nBytesRead = nBytesRead + 2 * count;
if count ~= MDH_NUMBEROFICEPROGRAMPARA
    ok = false;
    return
end

%  PACKED_MEMBER( uint16_t,     aushFreePara[MDH_FREEHDRPARA] );                 // 136:( 8) free parameter                                 
[header.aushFreePara, count]               = fread(fid, [1 MDH_FREEHDRPARA],  'uint16');
nBytesRead = nBytesRead + 2 * count;
if count ~= MDH_FREEHDRPARA
    ok = false;
    return
end

%  PACKED_MEMBER( uint8_t,      cApplicationParameter[44]     );                 // 144:(44) unused (padding to next 192byte alignment )    
[header.cApplicationParameter, count]      = fread(fid, [1 44],  'uint8');
nBytesRead = nBytesRead + 1 * count;
if count ~= 44
    ok = false;
    return
end

%  PACKED_MEMBER( uint32_t,     ulCRC                         );                 // 188:( 4) CRC 32 checksum
[header.ulCRC, count]                      = fread(fid, 1,      'uint32');
if count == 1
    nBytesRead = nBytesRead + 4;
else
    ok = false;
    return
end

%} sScanHeader;                                                                  // total length: 6 x 32 Byte (192 Byte)                

