% READ_MEAS_VD11 - read meas.dat file along all dimensions for software version VD11
%
% function [raw noise ref phasecor pc_order centerlines lastheader rest] = ...
%           read_meas_vb13(filename [,cFile] [,mode] [,undoOS] [,repWise] [,average] [, extRefOnly])
%
%   raw             : rawdata
%
%   noise           : noise adjust scan (for iPAT measurements)
%
%   ref             : iPAT reference lines (*currently not filled*)
%
%   phasecor        : TSE / EPI phase correction scans (*not thoroughly
%                       tested, use with care!*)
%
%   pc_order        : experimental output
%
%   centerlines     : line index / indices of k-space center lines
%                       (may contain an additional 1 for TSE due to refScan)
%
%   lastheader      : last measurement data header as read by this function
%                       (for writing / duplicating meas.out)
%
%   rest            : binary data for the rest of the meas.out file
%                       (for writing / duplicating meas.out)
%
%   cFile           : index of file in super-raid [1...nFiles], default: 1
%
%   modes
%   - 'STD'         : all loop counters are used
%   - 'TSE'         : segments ignored since redundant with lines (default)
%   - 'DRY'         : only print max loop counters
%   - 'ADJ_FRE'     : read frequency adjustment scan(s) from the AdjFre measurement
%   - 'ADJ_MDS_FRE' : read frequency adjustment scans from the AdjMds measurement
%   - 'UTE'         : special mode reading selected echo and channel indices (may also be useful for 128channel data sets)
%
%   undoOS          : 0 = preserve ADC oversampling (2x); 1 = remove (default)
%
%   repWise         : 1 = put multiple measurements (repetitions) into
%                       separate files (for large data sets), default = 0
%                     2 = put multiple phases into separate files
%
%   average         : 0 = preserve averages in their own matrix dimension
%                     1 = accumulate raw data across AVE dimension (default)
%
%   extRefOnly      : 1 = extract only "EXTRA" reference lines for iPAT
%
%
% Stephan.Kannengiesser@siemens.com
%
% -----------------------------------------------------------------------------
%   Copyright (C) Siemens AG 2009  All Rights Reserved. Confidential.
% -----------------------------------------------------------------------------
%
function [raw, noise, ref, phasecor, pc_order, centerlines, lastheader, rest, phasestab] = ...
    read_meas_vd11(filename, cFile, mode, undoOS, repWise, average, extRefOnly)

disp(['* This is read_meas_vd11, version 1.1.2'])

% Defines
PATNAMLEN                = 64; % Max length of patient name
PROTNAMLEN               = 64; % Max length of protocol name
MR_PARC_RAID_ALLDATA     = 0;  % normal daVinci MDH file (*.dat)
MR_PARC_RAID_MDHONLY     = 1;  % �compact?MDH file (Messdaten removed) (*.mdh)
MR_PARC_RAID_HDONLY      = 2;  % MDH file only with the header (meas.evp, phoenix.evp)
MR_PARC_RAID_LEGACY_THR  = 32; % pre VD11A MDH with empty measurement header
% > MR_PARC_RAID_LEGACY_THR    - pre VD11A MDH with (meas.evp, phoenix.evp,?

MRPARCRAID_SECT_ALIGN    = 512; % data size alignment

% find default parameters
% - filename is first
% - to facilitate the adding of later parameters, work in order only
currArg = 2;
if nargin < currArg
  cFile = 1;
end
currArg = currArg + 1;
if nargin < currArg
  mode = 'TSE';
end
currArg = currArg + 1;
if nargin < currArg
  undoOS = 1;
end
currArg = currArg + 1;
if nargin < currArg
  repWise = 0;
end
currArg = currArg + 1;
if nargin < currArg
  average = 1;
end
currArg = currArg + 1;
if nargin < currArg
  extRefOnly = 0;
end

% Shortcuts
is_adj_mds_fre = strcmpi(mode, 'ADJ_MDS_FRE') | strcmpi(mode, 'ADJ_FRE_MDS');

% first pass: open file and find out about dimensions as well as headers
maxLC  = zeros([1 14]);
maxCOL = 0;
maxCHA = 0;

centerlines = zeros([1 0]);
centerpars  = zeros([1 0]);

% Open the file
fid = fopen(filename,'rb');

% byte count - for alignment
bc = 0;

% First four bytes contain the version
superId = fread(fid, 1, 'uint32'); bc = bc + 4;

if superId < 32
   disp(['* first 4 bytes: ' num2str(superId) ' -> this is a VD11 data file']); 
else
   disp(['* first 4 bytes: ' num2str(superId) ' -> this is NOT a VD11 data file, aborting...']); 
   raw = [];
   return
end

% Second four bytes contain number of files in "super-RAID"
nFiles = fread(fid, 1, 'uint32'); bc = bc + 4;

disp(['* super-RAID file contains ' num2str(nFiles) ' data file(s)']);

if cFile > nFiles
    disp(['*** cFile (' num2str(cFile) ') > nFiles (' num2str(nFiles) ...
        '), aborting']);
    raw = [];
    return
end

% Loop over measurement data file headers
measId  = zeros([nFiles 1]);
fileId  = zeros([nFiles 1]);
fileOff = zeros([nFiles 1]);
fileLen = zeros([nFiles 1]);
for cF = 1:nFiles

    % Read initial information from this measurement file
    measId(cF)  = fread(fid, 1, 'uint32'); bc = bc + 4; % Used for MDH file identification
    fileId(cF)  = fread(fid, 1, 'uint32'); bc = bc + 4; % ? Number of measurements in file
    fileOff(cF) = fread(fid, 1, 'uint32'); bc = bc + 4; % offset of measurement from start
    fileLen(cF) = fread(fid, 1, 'uint32'); bc = bc + 4; % length of measurement
    dummy1         = fread(fid, 1, 'uint32'); bc = bc + 4; % experimental: 
    dummy2         = fread(fid, 1, 'uint32'); bc = bc + 4; % experimental: 
    
    disp(['* File # ' num2str(cF)]);
    disp(['- measId  : ' num2str(measId(cF))]);
    disp(['- fileId  : ' num2str(fileId(cF))]);
    disp(['- fileOff : ' num2str(fileOff(cF))]);
    disp(['- fileLen : ' num2str(fileLen(cF))]);
    disp(['- dummy1  : ' num2str(dummy1)]);
    disp(['- dummy2  : ' num2str(dummy2)]);
    
    patName    = fread(fid, [1 PATNAMLEN],  'char'); bc = bc + PATNAMLEN;  % Patient name
    protName   = fread(fid, [1 PROTNAMLEN], 'char'); bc = bc + PROTNAMLEN; % Protocol name
    
    disp(['- patName : ' patName]);
    disp(['- protName: ' protName]);

end % for cF

% Align superHeader to MRPARCRAID_SECT_ALIGN
% - this is probably unnecessary, we have the offset value
nBytesNeeded = MRPARCRAID_SECT_ALIGN * ceil(bc/MRPARCRAID_SECT_ALIGN);

disp(['* byte count so far is ' num2str(bc) ' bytes -> patch to multiple of ' ...
    num2str(MRPARCRAID_SECT_ALIGN) ' -> ' num2str(nBytesNeeded)]);

fread(fid, nBytesNeeded-bc, 'uint8');
bc = nBytesNeeded;

disp(['- file is positioned at ' num2str(ftell(fid))]);

% Don't continue for more than one file (at the moment)
%if ( nFiles > 1 )
%    disp('* currently only 1 file supported, aborting...');
%    raw = [];
%    return
%end

% Do we count the super header?
nBytesToRead = fileOff(cFile) - bc;

disp(['* continue to read until offset of measurement sub-file ' num2str(cFile) ...
    ' = ' num2str(fileOff(cFile)) ' -> ' num2str(nBytesToRead) ' bytes']);

fread(fid, nBytesToRead, 'uint8');

% Now there should be an ordinary measurement, including its own header
%  (evp etc.)
% -> skip that similar to a VB11 type measurement
nBytesFileHeader = fread(fid, 1, 'uint32');

disp(['* Measurement ' num2str(cFile) ' has a header of ' num2str(nBytesFileHeader) ' bytes length']);

fread(fid, nBytesFileHeader-4, 'uint8');%kuanbiu2 fix

% Finally, we should have reached the first data scan!

% Read one scan header
%[scanheader, ok, nBytesRead] = read_vd11_scanheader(fid);

%disp(['* Reading scan header, ok = ' num2str(ok) ', nBytesRead = ' num2str(nBytesRead)]);
%disp(['- scanheader.ushSamplesInScan: ' num2str(scanheader.ushSamplesInScan)]);
%disp('- Scan header:'); scanheader

readMore    = 1;
nScansRead  = 0;
nTablePos   = 0;
global tablePos
tablePos    = [];
while readMore
  nScansRead  = nScansRead + 1;
  [header ok] = read_vd11_scanheader(fid);
  if ok
    mask        = evalInfoMask(header.aulEvalInfoMask(1));
  else 
    disp(['*** header read not ok after ' num2str(nScansRead) ' scans - aborting read'])
  end
  readMore    = ~mask.MDH_ACQEND && ok;
  if readMore
    % just dummy read the data!
      
    % Debug only
    header2 = header;
    
    % Synchdata - no channel depencence?
    if mask.MDH_SYNCDATA
        
        nBytesSyncdata = bitand(header.ulFlagsAndDMALength, hex2dec('1FFFFFF')) - 192; % 192 = length scanheader
        
        if 0 % debug
            disp(['* reading ' num2str(nBytesSyncdata) ' bytes of SYNCDATA'])
            nLenSyncDat = fread( fid, [1 1], 'int16' );
            nBytesSyncdata = nBytesSyncdata - 2;
            nSyncDatSkipBytes = nLenSyncDat+72;
            disp(['- TK predicts ' num2str(nSyncDatSkipBytes) ' bytes to read'])
            %if nSyncDatSkipBytes ~= nBytesSyncdata
            %    disp(['-> using ' num2str(nSyncDatSkipBytes)]);
            %    nBytesSyncdata = nSyncDatSkipBytes - 2;
            %end
        end
        
        a    = fread(fid, [1 nBytesSyncdata], 'uint8');
        
        if any(size(a) ~= [1 nBytesSyncdata])
            disp('256 *** ERROR reading from file (SYNCDATA)')
            disp(['- nBytesSyncdata: ' num2str(nBytesSyncdata) ...
                ' / length(a): ' num2str(length(a))]);
            disp('- header:');
            header
            readMore = 0;
            break
        end % if
        
        continue
        
    else

        if header.ushUsedChannels < 1
            disp(['*** header.ushUsedChannels = ' num2str(header.ushUsedChannels) ...
                ' - this is unexpected, aborting']);
            disp('- header:');
            header
            readMore = 0;
            break
        end
%         disp(['277 kuanbiu2 check - file is positioned at '
%         dec2hex(ftell(fid))]);
        % New: channel-wise
        for cCha = 1:header.ushUsedChannels
            
            channelheader = read_vd11_channelheader(fid);
            %keyboard
                %disp(['* reading ' num2str(header.ushSamplesInScan) ' complex floats'])
                a    = fread(fid, [2 header.ushSamplesInScan], 'float32');
             if 0 %kuanbiu2 debug
                aData =complex(a(1,:),a(2,:));
                subplot(1,2,1);
                plot(abs(aData(1,:)));
                subplot(1,2,2);
                plot((angle(aData(1,:))/pi)*180.0);
                pause;
            end
            if any(size(a) ~= [2 header.ushSamplesInScan])
                disp('294 *** ERROR reading from file')
                disp(['- channel ' num2str(cCha) ' of ' num2str(header.ushUsedChannels) ...
                    ': ' num2str(header.ushSamplesInScan) ' samples in scan, but only ' ...
                    num2str(size(a,2)) ' read']);
                disp('- header:');
                header
                readMore = 0;
                break
            end % if
            
        end % for cCha
        
    end % if Syncdata
    
    if  (extRefOnly && ~mask.MDH_PATREFSCAN) || (mask.MDH_SYNCDATA)
        continue
    end
    
    % Treat AdjFre data
    % - there may be several scans, not distinguished by loop counters
    %   -> we try to use the scan counter
    if strcmpi(mode, 'ADJ_FRE')
        header.sLC(7) = header.ulScanCounter-1; % 7 = repetition
    end
    
    % Treat AdjMds data
    % - there are several types of data in here, distinguished by the free parameter settings
    if is_adj_mds_fre
        if header.aushFreePara(2) ~= 2
            continue
        end
    end
    
    % log table positions
    nTablePos = nTablePos + 1;
    %tablePos(nTablePos) = header.ushPTABPosNeg;
    tablePos(nTablePos) = header.lPTABPosZ;
    
    % Update max. indices
    maxLC  = max([header.sLC ; maxLC]);
    maxCOL = max(maxCOL, header.ushSamplesInScan);
    maxCHA = max(maxCHA, channelheader.ulChannelId);
    
    %centerline = header.ushKSpaceCentreLineNo;
    %disp(centerline)
    
    if (isempty(centerlines)) || ...
            ~any(centerlines == repmat(header.ushKSpaceCentreLineNo+1, ...
            size(centerlines)))
        centerlines = [centerlines header.ushKSpaceCentreLineNo+1];
    end
    
    if (isempty(centerpars)) || ...
          ~any(centerpars == repmat(header.ushKSpaceCentrePartitionNo+1, ...
                                     size(centerpars)))
      centerpars = [centerpars header.ushKSpaceCentrePartitionNo+1];
    end
     
  end % readMore
end % while

disp([num2str(nScansRead) ' scans read'])

lastheader = header;

% remember dimensions
dimNames = ['LIN'; 'ACQ'; 'SLC'; 'PAR'; 'ECO'; 'PHS'; 'REP'; 'SET'; 'SEG'; ...
            'IDA'; 'IDB'; 'IDC'; 'IDD'; 'IDE'];
dimNames = ['COL'; 'CHA'; dimNames];

%size(dimNames)
%size(maxLC)

disp('* sizes:')
disp([dimNames repmat(': ',[16 1]) num2str([maxCOL maxCHA+1 maxLC+1].')])
centerlines
centerpars

if strcmpi(mode, 'DRY')
  
  % Try to read the rest of the file
  [rest count] = fread(fid, [1 inf], 'char');

  disp(['* read ' num2str(count) ' extra bytes after last header'])
  
  fclose(fid);
  
  raw = [];
  
  return
  
else
  fclose(fid);
end % if DRY

if strcmpi(mode, 'EPI')
  disp('- EPI: ignoring segments and sets')
  maxLC(9) = 0;
  maxLC(8) = 0;
end
if strcmpi(mode, 'TSE') || strcmpi(mode, 'UTE')
  disp('- TSE/UTE: ignoring segments')
  maxLC(9) = 0;
end

if strcmpi(mode, 'UTE')
  % HACK for UTE
  cEco = 0;
  cCha = 0;
  nCha = 64;
  %disp(['*** UTE HACK: read only channel index ' num2str(cCha) ' and echo index ' num2str(cEco)]);
  disp(['*** UTE HACK: read only channel index 0...' num2str(nCha-1) ' and echo index ' num2str(cEco)]);
  maxLC(5) = 0; % echo
  %maxCHA   = 0; % channel
  maxCHA   = nCha-1; % channel
end

if average
  disp('- average: return only averaged data')
  maxLC(2) = 0;
end
if repWise == 1
  %disp(['- repWise: returning only last repetition, writing 1 to ' ...
  %      num2str(repWise) ' to disk!'])
  disp(['- repWise: returning only last repetition, writing others to disk!'])
  maxLC(7) = 0;
end
currRepCtr = 0;
if repWise == 2
  disp(['- phsWise: returning only last phase, writing others to disk (may need to re-load!)'])
  phaseWritten = zeros([maxLC(6)+1 1]);
  maxLC(6) = 0;
end
currPhsCtr = 0;

% Now read for real (flags???)
if undoOS
  raw_size = [maxCOL/2 maxCHA+1 maxLC+1];
else
  raw_size = [maxCOL maxCHA+1 maxLC+1];
end
nCmplx = prod(raw_size);
disp(['* trying to allocate MBytes: ' ...
      num2str(16*nCmplx/1024/1024)])

%pause(2)

% NOTE: some loop counters are not distinct labels, but
%       provide additional information without requiring
%       more storage, e.g., segments!
try
  raw      = zeros(raw_size);
  noise    = zeros([maxCOL 0]);
  ref      = zeros([maxCOL 0]);
  phasecor = zeros([maxCOL 0]);
  phasestab = zeros([maxCOL 0]);
  pc_order = zeros([1      0]); % slc/par
catch errInfo
  disp(['*** Error (' errInfo ') allocating memory - aborting'])
  return
end

fid      = fopen(filename, 'rb');

fread(fid, fileOff(cFile), 'uint8');

fHead    = fread(fid, [1 nBytesFileHeader], 'uint8');%kuanbiu2 fix here

readMore = 1;

while readMore
  [header ok]    = read_vd11_scanheader(fid);
  if ~ok
    disp(['... header read not ok on second run - aborting read'])
  else
    %disp(['header ok, mask.MDH_ACQEND = ' num2str(mask.MDH_ACQEND)])
  end
  readMore       = ok;
  
  if ok
	  [mask taglist] = evalInfoMask(header.aulEvalInfoMask(1));

    readMore       = ~mask.MDH_ACQEND;
	  
	  % check for new repetition
	  if (repWise==1) & (header.sLC(7) ~= currRepCtr)
	    disp(['- dumping repetition ' num2str(currRepCtr+1)])
	    save(sprintf('raw_rep%05d',currRepCtr+1), 'raw');
	    currRepCtr = header.sLC(7);
	
	    % External references usually come first
	    if extRefOnly
	      break
	    end
	    
	    % New: we might only want the first repWise measurements
	    % - superseded by phsWise
	    %if currRepCtr >= repWise
	    %  break
	    %end
	
	    raw = zeros(raw_size);
	    
	  end % if (repWise==1) & (header.sLC(7) ~= currRepCtr)
	  
	  % check for new phase
	  if (repWise==2) && (header.sLC(6) ~= currPhsCtr)
	    disp(['- dumping phase ' num2str(currPhsCtr+1)])
	    save(sprintf('raw_phs%05d',currPhsCtr+1), 'raw');
	    phaseWritten(currPhsCtr+1) = 1;
	    currPhsCtr = header.sLC(6);
	
	    % External references usually come first
	    if extRefOnly
	      break
	    end
	    
	    if phaseWritten(currPhsCtr+1)
	      disp(['- re-reading phase ' num2str(currPhsCtr+1)])
	      load(sprintf('raw_phs%05d',currPhsCtr+1), 'raw');
	    else
	      raw = zeros(raw_size);
	    end
	    
	  end % if (repWise==2) & (header.sLC(6) ~= currRepCtr)
	  
	end % if ok
  
  if readMore
        
    if mask.MDH_SYNCDATA
        nBytesSyncdata = bitand(header.ulFlagsAndDMALength, hex2dec('1FFFFFF')) - 192; % 192 = length scanheader
        a    = fread(fid, [1 nBytesSyncdata], 'uint8');
        if any(size(a) ~= [1 nBytesSyncdata])
            disp('528 *** ERROR reading from file (SYNCDATA)')
            readMore = 0;
        end % if
        continue
    end % if syncdata

    % New: channel-wise
%     disp(['535 kuanbiu2 check - file is positioned at ' dec2hex(ftell(fid))]);
    for cCha = 1:header.ushUsedChannels
        
        channelheader = read_vd11_channelheader(fid);
            a    = fread(fid, [2 header.ushSamplesInScan], 'float32');
            if any(size(a) ~= [2 header.ushSamplesInScan])
                disp('541 *** ERROR reading from file')
                readMore = 0;
                break
            end % if
    
        if strcmpi(mode, 'ADJ_FRE')
            header.sLC(7) = header.ulScanCounter-1; % 7 = repetition
        end
        
        % Patch AdjMds data
        if is_adj_mds_fre
            if header.aushFreePara(2) ~= 2
                %disp(['ignoring ' num2str(header.aushFreePara(2))])
                continue
            else
                %disp('ADJ_MDS_FRE')
            end
        end
        
        if strcmpi(mode, 'UTE')
            % UTE HACK
            if header.sLC(5) ~= cEco
                continue
            else
                header.sLC(5) = 0;
            end
            %if header.ushChannelId ~= cCha
            if channelheader.ulChannelId >= nCha
                continue
            else
                %header.ushChannelId = 0;
            end
        end
        
        b      = a(1,:) + 1i.*a(2,:);
        % reverse if necessary
        if mask.MDH_REFLECT
            b = b(1,end:-1:1);
        end % reverse
        % distinguish between attributes
        if mask.MDH_NOISEADJSCAN
            % very few, and first: don't worry about copying
            noise = [noise b.'];
        elseif mask.MDH_PHASCOR
            % process the same as noise
            phasecor = [phasecor b.'];
            % - but remember the slices
            pc_order = [pc_order header.sLC(3)+1];
        elseif mask.MDH_REFPHASESTABSCAN || mask.MDH_PHASESTABSCAN
            phasestab = [phasestab b.'];
        else % if 0
            % assume this is an imaging scan
            if undoOS
                lb = length(b);
                b  = fftshift(ifft(fftshift(b(:))));
                %b  = b(lb/4+1:3*lb/4);
                b  = fftshift(fft(fftshift(b(lb/4+1:3*lb/4))));
            end
            lb = length(b);
            f = (raw_size(1)-lb)/2+1;
            t = (raw_size(1)+lb)/2;
            if strcmpi(mode, 'TSE') || strcmpi(mode, 'UTE')
                header.sLC(9) = 0;
            end
            if strcmpi(mode, 'EPI')
                header.sLC(9) = 0;
                header.sLC(8) = 0;
            end
            if repWise==1
                header.sLC(7) = 0;
            end
            if repWise==2
                header.sLC(6) = 0;
            end
            if extRefOnly
                if (~mask.MDH_PATREFSCAN)
                    continue
                end
            else
                %disp([mask.MDH_PATREFSCAN mask.MDH_PATREFANDIMASCAN])
                % - DEBUG: suppress inplace refLines
                %if mask.MDH_PATREFSCAN & (~mask.MDH_PATREFANDIMASCAN)
                %  continue
                %end
            end
            if average
                header.sLC(2) = 0;
            end
            %if any([header.ushSamplesInScan header.ushChannelId+1 header.sLC] ...
            if any([lb channelheader.ulChannelId+1 header.sLC+1] ...
                    > raw_size)
                % index out of range - ignoring
                disp('** ignoring index')
                continue
            end
            %disp([num2str([header.ushChannelId+1 header.sLC])])
            try
                if average && header.sLC(2) > 0
                    raw( f:t, channelheader.ulChannelId+1, ...
                        header.sLC(1)+1, ...
                        header.sLC(2)+1, ...
                        header.sLC(3)+1, ...
                        header.sLC(4)+1, ...
                        header.sLC(5)+1, ...
                        header.sLC(6)+1, ...
                        header.sLC(7)+1, ...
                        header.sLC(8)+1, ...
                        header.sLC(9)+1, ...
                        header.sLC(10)+1, ...
                        header.sLC(11)+1, ...
                        header.sLC(12)+1, ...
                        header.sLC(13)+1, ...
                        header.sLC(14)+1 ) = ...
                        raw( f:t, channelheader.ulChannelId+1, ...
                        header.sLC(1)+1, ...
                        header.sLC(2)+1, ...
                        header.sLC(3)+1, ...
                        header.sLC(4)+1, ...
                        header.sLC(5)+1, ...
                        header.sLC(6)+1, ...
                        header.sLC(7)+1, ...
                        header.sLC(8)+1, ...
                        header.sLC(9)+1, ...
                        header.sLC(10)+1, ...
                        header.sLC(11)+1, ...
                        header.sLC(12)+1, ...
                        header.sLC(13)+1, ...
                        header.sLC(14)+1 ) + b(:);
                else
                    raw( f:t, channelheader.ulChannelId+1, ...
                        header.sLC(1)+1, ...
                        header.sLC(2)+1, ...
                        header.sLC(3)+1, ...
                        header.sLC(4)+1, ...
                        header.sLC(5)+1, ...
                        header.sLC(6)+1, ...
                        header.sLC(7)+1, ...
                        header.sLC(8)+1, ...
                        header.sLC(9)+1, ...
                        header.sLC(10)+1, ...
                        header.sLC(11)+1, ...
                        header.sLC(12)+1, ...
                        header.sLC(13)+1, ...
                        header.sLC(14)+1 ) = b(:);
                end
            catch errInfo
                disp(['*** SIZE too large (' errInfo '), aborting'])
                readMore = 0;
                f
                t
                header.ushChannelId+1
                header.sLC.'+1
                break
            end % try
            % else
        end % if mask....
        
    end % for cCha

  end % if readMore
  
end % while readMore

% Clean up 

if (repWise==1)
  disp(['- finally: dumping repetition ' num2str(currRepCtr+1)])
  save(sprintf('raw_rep%05d',currRepCtr+1), 'raw');
end

if (repWise==2)
  disp(['- finally: dumping phase ' num2str(currPhsCtr+1)])
  save(sprintf('raw_phs%05d',currPhsCtr+1), 'raw');
end

fclose(fid);
