#-------------------------------------------------------------------------------
# Name:        Read_Data_Files/ReadRawData
# Purpose:
#
# Author:      pottanjz
#
# Version:     0.1
# Created:     2014-07-09
# Copyright:   (c) Siemens AG Healthcare Sector 2014
# Licence:     restricted
#-------------------------------------------------------------------------------
import sys
import io
import struct
import re
import numpy as np

class RawdataReader:

    lcnames=['Meas', 'Line', 'Acq', 'Slice', 'Part', 'Echo', 'Phase', 'Rept', 'Set', 'Seg', 'Ida', 'Idb', 'Idc', 'Idd', 'Ide', 'Ch', 'Samples']

    def __init__(self, filename):
        self.__raid_entry_list=[]
        self._preselect=None
        self.__matrixIds=None
        self.__buffers={}
        self.__isVDdata=True
        self.__f=open(filename, 'rb')
        # read first 8 bytes
        a=self.__f.read(8)
        (hdsize, count)=struct.unpack_from("=LL", a)
        if (hdsize >= 32):
            print ("This is VB-type rawdata")
            self.__isVDdata=False
            self.read_vb_header_info()
        else:
            print ("This is VD-type rawdata")
            self.read_vd_multiraid_header()

    def __del__(self):
        self.__raid_entry_list=None
        self._preselect=None
        self.__matrixIds=None
        self.__buffers=None
        self.__isVDdata=None
        self.__f.close()

    def set_preselect(self, preselect):
        """
        Sets the loop counters which can be filtered when reading rawdata

        The following filters can be used in the dictionary parameter:
        'Meas', 'Line', 'Acq', 'Slice', 'Part', 'Echo', 'Phase', 'Rept', 'Set', 'Seg', 'Ida', 'Idb', 'Idc', 'Idd', 'Ide', 'Ch'

        Each value for the above keys may contain a comma separated list of indices to be taken
        from the rawdata. Ranges can be specified with a ':' and do not contain last element (like in Python):

        Example: {'Line': '0:4,7,8-10'} will select the lines 0-3, 7, 8 and 9

        The filter 'Meas' can either specify a filter for a specific MeasID or a measurement index in the rawdata set

        Example: {'Meas':'0:2,4,6,MID278'} will process the measurement sets 0-1, 4, 6 and the one with the MID278

        The dictionary may also specify a key 'EvalInfoMask' to allow for filtering of specific lines. The parameter for the
        EvalInfoMask is a tuple of a quad word. The first value specifies the mask, the second value the expected result.

        Example: To only use lines with the FirstScanInSlice bit in the MDH set use
                {'EvalInfoMask': (0x10000000, 0x10000000) . If you want to use all lines except for the ones
                where this bit is set use
                {'EvalInfoMask': (0x10000000, 0)
        """
        self._preselect=preselect

    def get_preselect(self):
        """
        Returns the current preselection dictionary

        see set_preselect for details
        """
        return(self._preselect)

    def get_matrixIds(self):
        """
        Returns the last matrixIds (list of lists)

        It contains the indices for the loop counters found (optionally after preselection (see preselect for details))
        in the rawdata set as a list of lists (the last entry of the list is a value and contains the max number of
        samples in a rawdata line
        """
        return(self.__matrixIds)

    def read_vb_header_info(self):
        """
        Reads the (header) info  from a VB rawdata file

        The method returns one entry for the __raid_entry_list
        entry is a tuple (measID, -1, offset, length, "", "")
        """
        if (self.__isVDdata):
            print ("ERROR: This is not a VB rawdata file")
            return
        self.__f.seek(0)
        a=self.__f.read(8)  # read first 8 bytes
        (hdsize, count)=struct.unpack_from("=LL", a)
        self.__f.seek(hdsize)  # move to first MDH
        a=self.__f.read(8)  # read first 8 bytes
        (dmaflags, mid)=struct.unpack_from("=LL", a)
        self.__f.seek(0, io.SEEK_END)
        filelen=self.__f.tell()
        self.__raid_entry_list=[]
        # length for VD data is length of data for one measurement, including buffers
        # so for VB rawdata we set it accordingly:
        # there is no multiraid header, so we set the start to 0 and the length of the first measurement
        # to the length of the file
        # (the dmalen of the buffer data in VD files is the equivalent of the header length in VB files)
        self.__raid_entry_list.append((mid, -1, 0, filelen, "", ""))


    def read_vd_multiraid_header(self):
        """
        Reads the MultiRaid header from a VD rawdata file

        The method returns a list of the RaidEntries is the rawdata file
        each entry is a tuple (measID, fileID, offset, length, patientName, protocolName)
        """
        if (not self.__isVDdata):
            print ("ERROR: This is not a VD rawdata file")
            return
        self.__f.seek(0)
        a=self.__f.read(9736)  # vd headers are always 9736 bytes long
        (hdsize, count)=struct.unpack_from("=LL", a)
        self.__raid_entry_list=[]
        curpos=8 # first 8 bytes have already been read
        for i in range(count):
            self.__raid_entry_list.append(struct.unpack_from("=LLQQ64s64s", a[curpos:]))
            curpos+=152

    def read_meas_header(self, raid_entry_id, req_buf_list=None):
        """
        Reads the meas_header specified by the raid_entry from a rawdata file

        The method expects the index of the measurement (e.g. 0 for the first measurement) as parameter
        and returns the dma_len of the corresponding measurement header

        If the optional parameter req_buf_list (string with regex) is set, it returns the bufferlist
        of all buffers matching the regex pattern

        This method should work for both VB and VD rawdata files
        """
        if (raid_entry_id>len(self.__raid_entry_list)):
            print ("ERROR: specified raid_entry_ID is larger than number of raid_entries found")
            return(None)
        raid_entry=self.__raid_entry_list[raid_entry_id]
        self.__f.seek(raid_entry[2])
        a=self.__f.read(8)
        (dma_len, num_buffers)=struct.unpack_from("=LL", a)
        if (req_buf_list == None):
            return(dma_len)
        a=self.__f.read(dma_len-8)
        buffer_list=[]
        curpos=0
        bufre=re.compile(req_buf_list)
        for buf in range(num_buffers):
            bufpart=a[curpos:].partition('\0')
            bufname=bufpart[0]
            (buflen, )=struct.unpack_from("=L", bufpart[2])
            if (bufre.search(bufname)):
                bufcontent=bufpart[2][4:4+buflen]
                buffer_list.append((bufname, buflen, bufcontent))
            curpos+=len(bufname)+5+buflen
        return(buffer_list)

    def __add_filter(self, filtstr):
        """
        Returns a list by evaluating the filterstr

        Example: for '0:5, 6, 8-10, MID675:MID677' the method returns [0, 1, 2, 3, 4, 6, 8, 9, 'MID675', 'MID676']
        """
        # remove all whitespaces from self._preselect
        filtstr=str(filtstr)
        # remove spaces, the array boundaries (if parameter had been passed as array)
        # and potential ' and " in the array
        filtstr=re.sub(r'[ \[\]\'"]', '', filtstr)
        MeasFilterList=re.split(r'[,;]',filtstr ) # split at all , or ;
        MeasIDFilter=set()
        for mfilt in MeasFilterList:
            tmplist=[]
            if (mfilt.startswith('MID')):
                startstr='MID'
                mfilt=re.sub('MID', '', mfilt)
            else:
                startstr=None
            if (mfilt.find(':')!=-1):  # a range is defined
                mfilt=re.sub(':', ',', mfilt)
                tmplist=eval('range({0})'.format(mfilt))
            else:                  # this seems to be a standard value
                tmplist.append(int(mfilt))
            if (startstr):
                tmplist=['{0}{1}'.format(startstr, id) for id in tmplist[:]]
            MeasIDFilter=MeasIDFilter.union(tmplist)
        return (MeasIDFilter)

    def __read_data(self, matrixIds=None):
        """
        Reads either the indices or the rawdata from a rawdata file

        If the object matrixIds is not supplied supplied, a list of lists is returned which contain the indices
        for the loop counters. If this list is taken and used as parameter matrixIds for a second call to this method,
        the rawdata is read and a numpy matrix containing the rawdata is returned.

        The read data can be limited by setting a preselection (see set_preselect) prior to calling this method.
        """
        lcnames=RawdataReader.lcnames
        lcmax=len(lcnames)-1
        lc=[]
        lcfilt=[]
        for id in range(lcmax):
            lc.append([])
            lcfilt.append(set())
        maxSamples=0
        maxChannels=0
        EvalInfoMaskFilter=None
        # check mode:
        # if matrixIds==None we are in read_indices mode
        # otherwise we are in read_rawdata_mode
        isReadRawDataMode=(matrixIds!=None)
        if (self._preselect):
            if ('EvalInfoMask' in self._preselect):
                EvalInfoMaskFilter=self._preselect['EvalInfoMask']
            lcfilt=[[] for i in range(lcmax)]
            for id in range(lcmax):  # the last entry ('Samples') cannot be limited
                if (lcnames[id] in self._preselect):
                    lcfilt[id]=self.__add_filter(self._preselect[lcnames[id]])
                else:
                    lcfilt[id]=set()
        # print ("Mode: {0}".format("ReadRawData" if (isReadRawDataMode) else "GetLoopCounters"))
        if (isReadRawDataMode):
            # we have to allocate the numpy array
            dimensions=[len(x) for x in matrixIds[:-1]]
            dimensions.append(matrixIds[-1])  # the samples dimension remains unchanged
            # check that there are no empty indices
            if (0 in dimensions):
                zerodim=dimensions.index(0)
                print ("ERROR: No index parameters specified for loop counter {0}".format(lcnames[zerodim]))
                return(None)
            # check that the data size is not too big, limit it to 2GB
            rawdata_size=dimensions[0]
            for dimension_len in dimensions[1:]:
                rawdata_size*=dimension_len
            rawdata_size*=8  # for complex float
            if (rawdata_size>=0xF0000000):
                print ("ERROR: rawdata_size (={0}) > max_rawdata_size ({1})".format(rawdata_size, 0xF0000000))
                print ("Used indices for calculating rawdata size:")
                print ("\n".join(["{0}: {1}".format(x, y) for x, y in zip(lcnames, dimensions)]))
                return(None)
            try:
                print ("trying to allocate memory for rawdata. Dimensions: {0}".format(",".join(map(str, dimensions))))
                rawdata=np.zeros(dimensions, dtype=np.complex64)
            except:
                print ("ERROR trying to allocate memory for rawdata. Dimensions: {0}".format(",".join(map(str, dimensions))))
                raise
        for measnum, raid_entry in enumerate(self.__raid_entry_list):
            if (self._preselect and (len(lcfilt[0])>0) and
                (measnum not in lcfilt[0]) and ('MID{0}'.format(raid_entry[0]) not in lcfilt[0])):
                # print ("Meas {0} (MID{1}) not in lcfilt ({2}, Len{3})".format(measnum, raid_entry[0], ",".join(map(str, lcfilt[0])), len(lcfilt[0])))
                continue
            dma_len=self.read_meas_header(measnum)  # length of buffers before rawdata
            self.__f.seek(raid_entry[2]+dma_len)       # move filepointer to beginning of rawdata of measurement
            curpos=dma_len
            # print ("Working on meas {0}, MID{1}".format(measnum, raid_entry[0]))
            while (curpos < raid_entry[3]):
                # read scanheader
                if (self.__isVDdata):
                    a=self.__f.read(192)
                    (ulFlagsAndDMALength,
                    lMeasUID,
                    ulScanCounter,
                    ulTimeStamp,
                    ulPMUTimeStamp,
                    ushSystemType,
                    ushPTABPosDelay,
                    lPTABPosX,
                    lPTABPosY,
                    lPTABPosZ,
                    ulReserved1,
                    ullEvalInfoMask,
                    ushSamplesInScan,
                    ushUsedChannels)=struct.unpack_from("=LlLLLHHlllLQHH1",a)
                    if (ullEvalInfoMask & 0x20 == 0x20):  # this is SYNCDATA, so skip it
                        mv_offset=(ulFlagsAndDMALength & 0xFFFFFF)-192  # dma length - already read 192bytes
                        self.__f.seek(mv_offset, io.SEEK_CUR)
                        curpos+=mv_offset
                        continue
                    sLC=list(struct.unpack_from("=14H",a[52:]))
                    curpos+=192
                else:
                    a=self.__f.read(128)
                    (ulFlagsAndDMALength,
                    lMeasUID,
                    ulScanCounter,
                    ulTimeStamp,
                    ulPMUTimeStamp,
                    ullEvalInfoMask,
                    ushSamplesInScan,
                    ushUsedChannels)=struct.unpack_from("=LlLLLQHH",a)
                    if (ullEvalInfoMask & 0x20 == 0x20):  # this is SYNCDATA, so skip it
                        mv_offset=(ulFlagsAndDMALength & 0xFFFFFF)-128  # dma length - already read 128bytes
                        self.__f.seek(mv_offset, io.SEEK_CUR)
                        curpos+=mv_offset
                        continue
                    sLC=list(struct.unpack_from("=14H",a[32:]))
                    # move filepointer back, so that the channel input can be handled easier (curpos does not change)
                    self.__f.seek(-128, io.SEEK_CUR)
                if (ullEvalInfoMask & 0x1 == 0x1):
                    break
                clc=[lMeasUID]
                clc.extend(sLC)
                # if (clc[1]==1):
                    # print ("CLC: {0}".format(",".join(map(str, clc))))
                # check if item has to be added
                if (self._preselect):
                    # check whether EvalInfoMask prevents adding
                    skip_adding=(EvalInfoMaskFilter!=None and (ullEvalInfoMask & EvalInfoMaskFilter[0] != EvalInfoMaskFilter[1]))
                    if (not skip_adding):
                        # EvalInfoMask allows adding, so check whether we have a filter that prevents adding
                        for id in range(1, lcmax-1):
                            if ((len(lcfilt[id])>0) and (clc[id] not in lcfilt[id])):
                                skip_adding=True
                                # if (clc[1]==1):
                                    # print ("Skipping because {0} not in ({1}) for {2}".format(clc[id], ",".join(map(str, lcfilt[id])), lcnames[id]))
                                break
                    if (skip_adding):
                        # this scan does not need to be added
                        # if (clc[1]==1):
                            # print ("Scan will not be added")
                        if (self.__isVDdata):
                            mv_offset=(32+ushSamplesInScan*8)*ushUsedChannels
                        else:
                            mv_offset=ushSamplesInScan*8*ushUsedChannels
                        self.__f.seek(mv_offset, io.SEEK_CUR)
                        curpos+=mv_offset
                        continue
                if (isReadRawDataMode):
                    # construct the index
                    lcptr=[]
                    index_not_found=False
                    for lcid in range(lcmax-1):  # do not check the channels yet as they are not part of the clc array
                        if (clc[lcid] in matrixIds[lcid]):
                            lcptr.append(matrixIds[lcid].index(clc[lcid]))    # find indices in indexmatrix
                        else:
                            index_not_found=True
                    if (index_not_found):
                        # at least one dimension could not be found, so we skip this part
                        if (self.__isVDdata):
                            mv_offset=(32+ushSamplesInScan*8)*ushUsedChannels
                        else:
                            mv_offset=ushSamplesInScan*8*ushUsedChannels
                        self.__f.seek(mv_offset, io.SEEK_CUR)
                        curpos+=mv_offset
                        continue
                    lcptr.append(0)  # default channel id
                    for chid in range(ushUsedChannels):
                        if (self.__isVDdata):
                            # read channelheader
                            b=self.__f.read(32)
                            (ushChId, )=struct.unpack_from("=H",b[24:])
                            if (ushChId in matrixIds[lcmax-1]):
                                lcptr[-1]=matrixIds[lcmax-1].index(ushChId)   # find channel index in indexmatrix
                                rawdata[tuple(lcptr)]=np.fromfile(self.__f, dtype=np.complex64, count=ushSamplesInScan)
                            else:
                                # channel not found in index, so skip this part
                                self.__f.seek(ushSamplesInScan*8, io.SEEK_CUR)
                            curpos+=32+ushSamplesInScan*8
                        else:  # VB data
                            # read channelheader
                            b=self.__f.read(128)
                            (ushChId, )=struct.unpack_from("=H",b[124:])
                            if (ushChId in matrixIds[lcmax-1]):
                                lcptr[-1]=matrixIds[lcmax-1].index(ushChId)   # find channel index in indexmatrix
                                rawdata[tuple(lcptr)]=np.fromfile(self.__f, dtype=np.complex64, count=ushSamplesInScan)
                            else:
                                # channel not found in index, so skip this part
                                self.__f.seek(ushSamplesInScan*8, io.SEEK_CUR)
                            curpos+=128+ushSamplesInScan*8
                else:  # !isReadRawDataMode
                    for id in range(lcmax-1):
                        if (clc[id] not in lc[id]):
                            # print ("Appending to loop counter")
                            lc[id].append(clc[id])
                    if (ushSamplesInScan>maxSamples):
                        maxSamples=ushSamplesInScan
                    if (ushUsedChannels>maxChannels):
                        for chid in range(ushUsedChannels):
                            if (self.__isVDdata):
                                # read channelheader
                                b=self.__f.read(32)
                                (ushChId, )=struct.unpack_from("=H",b[24:])
                                if (ushChId not in lc[-1] and ((len(lcfilt[-1])==0) or (ushChId in lcfilt[-1]))):
                                    lc[-1].append(ushChId)
                                # advance filepointer by rawdata
                                mv_offset=ushSamplesInScan*8
                                self.__f.seek(mv_offset, io.SEEK_CUR)
                                curpos+=32+mv_offset
                            else:
                                # read channelheader
                                b=self.__f.read(128)
                                (ushChId, )=struct.unpack_from("=H",b[124:])
                                if (ushChId not in lc[-1] and ((len(lcfilt[-1])==0) or (ushChId in lcfilt[-1]))):
                                    lc[-1].append(ushChId)
                                # advance filepointer by rawdata
                                mv_offset=ushSamplesInScan*8
                                self.__f.seek(mv_offset, io.SEEK_CUR)
                                curpos+=128+mv_offset
                        maxChannels=ushUsedChannels
                    else:
                        if (self.__isVDdata):
                            mv_offset=(32+ushSamplesInScan*8)*ushUsedChannels
                        else:
                            mv_offset=(128+ushSamplesInScan*8)*ushUsedChannels
                        self.__f.seek(mv_offset, io.SEEK_CUR)
                        curpos+=mv_offset
        if (isReadRawDataMode):
            multidim=[]
            dimnames=[]
            for id, dim in enumerate(dimensions):
                if (dim>1):
                    multidim.append(dim)
                    dimnames.append(lcnames[id])
            rawdata=rawdata.reshape(tuple(multidim))
            print ("Rawdata size: {0}: {1}".format(' x '.join(dimnames), ' x '.join([str(x) for x in multidim])))
            return(rawdata)
        else:
            lc.append(maxSamples)
            return(lc)

    def read_meas_indices(self, preselect=None):
        """
        Reads the indices of a measurement in a meas_rawdata file

        The method returns a list with unique indices for each loop counter.
        This information is required for allocating the memory for the rawdata array
        An optional dictionary can be passed which filters the indices (see set_preselect)

        The samples can not be filtered and the last element of the returned object contains the max number of samples
        in a line

        the object returned by the function is a list of lists (last element (max. number of samples) is a value and not a list)
        """
        # print ("Trying to read indices")
        if (preselect):
            self._preselect=preselect.copy()
        self.__matrixIds=self.__read_data()
        return(self.__matrixIds)

    def read_meas_rawdata(self, matrixIds=None, preselect=None):
        """
        Reads the rawdata of a measurement in a meas_rawdata file

        The method returns a multidimensional numpy array.

        If the optional parameter matrixIds is supplied the method only reads the elements specified in the matrixIds.
        If it is not supplied, the method loops over the rawdata twice to first retrieve the indices and then
        read the corresponding rawdata.

        The optional parameter preselect allows to preselect the data to be read in (see set_preselect for details)

        """
        # print ("Trying to read rawdata")
        if (preselect):
            self._preselect=preselect.copy()
        if (matrixIds):
            self.__matrixIds=matrixIds
        else:
            self.__matrixIds=self.__read_data()
        return(self.__read_data(self.__matrixIds))

    def get_protocol(self, measnum=None):
        """
        Returns the measYAPS protocol for the specified measurement

        The measurement may either be specified by the index in the rawdata file or by the MID (e.g. 'MID376'). If no
        measnum has been supplied it defaults to the first protocol of the current preselection (see set_preselect for details).
        If the preselect has not been set it defaults to the first protocol in the rawdata set

        The method returns the first protocol found in the list, or '' if no protocol was found
        """
        if (measnum):
            idlist=self.__add_filter(measnum)
        else:
            # no measnum, so take it from the current preselection
            if (self._preselect and 'Meas' in self._preselect):
                idlist=self._preselect['Meas']
            else:
                idlist=range(len(self.__raid_entry_list))
            idlist=self.__add_filter(idlist)
        # now replace 'MID' entries with the corresponding index number of the dataset
        idlist=list(idlist)
        measIDvec=['MID{0}'.format(x[0]) for x in self.__raid_entry_list]
        id=0
        for iditem in idlist[:]:
            if (str(iditem).startswith('MID')):
                if (iditem in measIDvec):
                    # item exists in measIDVec, so we take the index
                    idlist[id]=measIDvec.index(iditem)
                else:
                    # item does not exist, so we delete it
                    del idlist[id]
                    id-=1
            else:
                # check if item is in valid range
                if (iditem < 0 or iditem>=len(self.__raid_entry_list)):
                    del idlist[id]
                    id-=1
            id+=1
        newlist=[]
        for item in idlist:
            if (item not in newlist):
                newlist.append(item)
        # now we should have a list of unique and valid item indices in newlist
        protname='MeasYaps'
        if (protname not in self.__buffers):
            self.__buffers[protname]=[None for id in range(len(self.__raid_entry_list))]
        retval=''
        for item in newlist:
                if (self.__buffers[protname][item]==None):
                    buffer=self.read_meas_header(item, protname)
                    if (len(buffer)>0 and len(buffer[0])>2):
                        self.__buffers[protname][item]=buffer[0][2]
                if (self.__buffers[protname][item]!=None):
                    retval=self.__buffers[protname][item]
        return(retval)

    def get_protocol_para(self, protocol_para=None, measnum=None):
        """
        Returns the measYAPS protocol parameter for the specified measurement

        The method returns a dictionary of the found parameters (passed as regex) of the specified measurement
        If no protocol_para has been specified, it returns a dictionary with all protocol parameters
        If no measnum has been specified, the rules for get_protocol protocol apply
        """
        protocol=self.get_protocol(measnum)
        # remove the ### ASCCONV from the protocol
        protocol=re.sub(r'### ASCCONV .*###', '', protocol)
        if (protocol_para==None):
            protocol_para=r'\S+'
        if (type(protocol_para)==str):
            protocol_para=[protocol_para]
        paralist=list(protocol_para)
        retdict={}
        float_re=re.compile(r'[\.e]')
        for para in paralist:
            prot_re=re.compile(r'('+para+r')\s*=\s*(.*)')
            for m in prot_re.finditer(protocol):
                value=m.group(2)
                if (value.startswith('"')):     # string value
                    retdict[m.group(1)]=value.strip('"')
                elif (value.startswith('0x')):  # hex value
                    retdict[m.group(1)]=int(float.fromhex(value))
                elif (float_re.search(value)):         # float value
                    retdict[m.group(1)]=float(value)
                else:                           # this should be an integer
                    retdict[m.group(1)]=int(value)
        return(retdict)

if __name__ == '__main__':
    ### internal test function to show capabilities when called directly
    import matplotlib.pyplot as plt

    # Test with VD rawdata:
    filename=r'I:\1_MR\1_HQMR\R&D\SW_SESO\Service-SW\Tools\Python\Read_Data_Files\meas_MID00749_FID01440_gre_ACC.dat'
    rawreader=RawdataReader(filename)
    preselect={}
    b=rawreader.read_meas_rawdata(preselect=preselect)
    plt.subplot(1,3,1)
    plt.plot(np.real(np.squeeze(b[126:131,1,:]).T))
    plt.subplot(1,3,2)
    plt.imshow(np.abs(np.squeeze(b[:,1,64:193]).T))
    plt.subplot(1,3,3)
    ima=np.fft.fftshift(np.fft.fft2(np.fft.fftshift(np.squeeze(b[:,1,:]))))[:,64:193]
    plt.imshow(np.abs(ima))
    plt.show()
    c=rawreader.get_protocol()
    d=rawreader.get_protocol_para(['tSequenceFileName', 'sTXSPEC.*flAmplitude', '.*lBaseResolution', '.*lPhaseEncodingLines', 'sTXSPEC.*bAmplitudeValid'])
    e=rawreader.get_protocol_para()
    print("Done")
    del rawreader

    filename=r'I:\1_MR\1_HQMR\R&D\SW_SESO\Service-SW\Tools\Python\Read_Data_Files\meas_MID00025_FID00587_spike_epi_LC_11slc.dat'
    rawreader=RawdataReader(filename)
    preselect={}
    b=rawreader.read_meas_rawdata(preselect=preselect)
    plt.subplot(1,3,1)
    plt.plot(np.real(np.squeeze(b[62:67,1,1,:]).T))
    plt.subplot(1,3,2)
    plt.imshow(np.abs(np.squeeze(b[:,1,1,32:97]).T))
    plt.subplot(1,3,3)
    ima=np.fft.fftshift(np.fft.fft2(np.fft.fftshift(np.squeeze(b[:,1,1,:]))))[:,32:97]
    plt.imshow(np.abs(ima))
    plt.show()
    c=rawreader.get_protocol()
    d=rawreader.get_protocol_para(['tSequenceFileName', 'sTXSPEC.*flAmplitude', '.*lBaseResolution', '.*lPhaseEncodingLines', 'sTXSPEC.*bAmplitudeValid'])
    e=rawreader.get_protocol_para()
    print("Done")
    del rawreader
